export const emotions = [
  { value: "happy", text: "Happy" },
  { value: "sad", text: "Sad" },
  { value: "angry", text: "Angry" },
  { value: "excited", text: "Excited" },
  { value: "thoughtful", text: "Thoughtful" },
];

export const activities = [
  { value: "movie", text: "Watch a movie/tv-series" },
  { value: "anime", text: "Watch an anime" },
  { value: "offline", text: "Find an offline activity" },
  { value: "book", text: "Read a book" },
  { value: "music", text: "Listen to some music" },
];

export const answers = {
  happy: {
    movie: [
      { name: "Across the Universe", description: "", image: "assets/tv.gif" },
      { name: "Adventure Time", description: "", image: "assets/tv.gif" },
      { name: "Glee", description: "", image: "assets/tv.gif" },
    ],
    anime: [
      {
        name: "The Disastrous Life of Saiki K",
        description: "",
        image: "assets/anime.gif",
      },
      { name: "Lucky Star", description: "", image: "assets/anime.gif" },
      { name: "K-on!", description: "", image: "assets/anime.gif" },
    ],
    offline: [
      {
        name: "Go to the cinema with friends",
        description: "",
        image: "assets/offline.gif",
      },
      {
        name: "Go to the bar with a friend",
        description: "",
        image: "assets/offline.gif",
      },
      {
        name: "Have a video call with your best friend",
        description: "",
        image: "assets/offline.gif",
      },
    ],
    book: [
      {
        name: "Sputnik Sweatheart by Haruki Murakami",
        description: "",
        image: "assets/reading.gif",
      },
      {
        name: "Killing Commendatore by Haruki Murakami",
        description: "",
        image: "assets/reading.gif",
      },
      {
        name: "Dandelion Wine by Ray Bradberry",
        description: "",
        image: "assets/reading.gif",
      },
    ],
    music: [
      {
        name: "Art Angels by Grimes",
        description: "",
        image: "assets/music.gif",
      },
      {
        name:
          "I like you when you sleep, for you are so beautiful yet unaware of it by the 1975",
        description: "",
        image: "assets/music.gif",
      },
      {
        name: "Thrill of the arts by Vulfpeck",
        description: "",
        image: "assets/music.gif",
      },
    ],
  },

  sad: {
    movie: [
      { name: "Euphoria", description: "", image: "assets/tv.gif" },
      { name: "What Dreams May Come", description: "", image: "assets/tv.gif" },
      { name: "La La Land", description: "", image: "assets/tv.gif" },
    ],
    anime: [
      { name: "Angel Beats", description: "", image: "assets/anime.gif" },
      { name: "A silent voice", description: "", image: "assets/anime.gif" },
      { name: "Nana", description: "", image: "assets/anime.gif" },
    ],
    offline: [
      { name: "Call a friend", description: "", image: "assets/offline.gif" },
      {
        name: "Visit a family member",
        description: "",
        image: "assets/offline.gif",
      },
      {
        name: "Go to a bookstore",
        description: "",
        image: "assets/offline.gif",
      },
    ],
    book: [
      {
        name: "Norwegian Wood by Haruki Murakami",
        description: "",
        image: "assets/reading.gif",
      },
      {
        name: "The Long Walk by Stephen King",
        description: "",
        image: "assets/reading.gif",
      },
      {
        name: "Waiting for the barbarians by Coetzee",
        description: "",
        image: "assets/reading.gif",
      },
    ],
    music: [
      {
        name: "Euphoria by Labirinth",
        description: "",
        image: "assets/music.gif",
      },
      {
        name: "Heaven's only Wishful by Mormor",
        description: "",
        image: "assets/music.gif",
      },
      {
        name: "Getaway by Red Hot Chilli Peppers",
        description: "",
        image: "assets/music.gif",
      },
    ],
  },

  angry: {
    movie: [
      { name: "BoJack Horseman", description: "", image: "assets/tv.gif" },
      { name: "Breaking bad", description: "", image: "assets/tv.gif" },
      { name: "Hannibal", description: "", image: "assets/tv.gif" },
    ],
    anime: [
      { name: "Death Note", description: "", image: "assets/anime.gif" },
      { name: "Durarara!!", description: "", image: "assets/anime.gif" },
      { name: "Attack on Titan", description: "", image: "assets/anime.gif" },
    ],
    offline: [
      {
        name: "Do some journaling",
        description: "",
        image: "assets/offline.gif",
      },
      { name: "Go for a jog", description: "", image: "assets/offline.gif" },
      {
        name: "Go to the gym",
        description: "",
        image: "assets/offline.gif",
      },
    ],
    book: [
      {
        name: "The Picture of Dorian Gray by Oscar Wilde",
        description: "",
        image: "assets/reading.gif",
      },
      {
        name: "Crime and Punishment by Fyodor Dostoevsky",
        description: "",
        image: "assets/reading.gif",
      },
      {
        name: "Idiot by Fyodor Dostoevsky",
        description: "",
        image: "assets/reading.gif",
      },
    ],
    music: [
      {
        name: "Little Dark Age by MGMT",
        description: "",
        image: "assets/music.gif",
      },
      {
        name: "Runaljod: Ragnarok by Wardruna",
        description: "",
        image: "assets/music.gif",
      },
      {
        name: "The Queen is Dead by The Smiths",
        description: "",
        image: "assets/music.gif",
      },
    ],
  },

  excited: {
    movie: [
      { name: "The Prestige", description: "", image: "assets/tv.gif" },
      { name: "Black Mirror", description: "", image: "assets/tv.gif" },
      { name: "Battlestar Galactica", description: "", image: "assets/tv.gif" },
    ],
    anime: [
      { name: "Soul Eater", description: "", image: "assets/anime.gif" },
      { name: "One Piece", description: "", image: "assets/anime.gif" },
      {
        name: "The Great Pretender ",
        description: "",
        image: "assets/anime.gif",
      },
    ],
    offline: [
      {
        name: "Go to a theme park with your friends",
        description: "",
        image: "assets/offline.gif",
      },
      { name: "Go to the zoo", description: "", image: "assets/offline.gif" },
      {
        name: "Play an online game with a friend",
        description: "",
        image: "assets/offline.gif",
      },
    ],
    book: [
      {
        name:
          "Hard-Boiled Wonderland and the End of the World by Haruki Murakami",
        description: "",
        image: "assets/reading.gif",
      },
      {
        name: "Fahrenheit 451 by Ray Bradbury",
        description: "",
        image: "assets/reading.gif",
      },
      {
        name: "1Q84 by Haruki Murakami",
        description: "",
        image: "assets/reading.gif",
      },
    ],
    music: [
      {
        name: "Miss Anthopocene by Grimes",
        description: "",
        image: "assets/music.gif",
      },
      {
        name: "Mr Finish Line by Vulfpeck",
        description: "",
        image: "assets/music.gif",
      },
      {
        name: "Point Pleasant by Brock Berrigan",
        description: "",
        image: "assets/music.gif",
      },
    ],
  },

  thoughtful: {
    movie: [
      { name: "10 Cloverfield Lane", description: "", image: "assets/tv.gif" },
      { name: "Get Out", description: "", image: "assets/tv.gif" },
      { name: "Sherlock", description: "", image: "assets/tv.gif" },
    ],
    anime: [
      {
        name: "Jungle wa Itsumo Hare nochi Guu",
        description: "",
        image: "assets/anime.gif",
      },
      { name: "Your name", description: "", image: "assets/anime.gif" },
      { name: "Haibane Renmei ", description: "", image: "assets/anime.gif" },
    ],
    offline: [
      {
        name: "Do some gardening",
        description: "",
        image: "assets/offline.gif",
      },
      { name: "Go for a walk", description: "", image: "assets/offline.gif" },
      {
        name: "Do some colouring",
        description: "",
        image: "assets/offline.gif",
      },
    ],
    book: [
      {
        name: "I'm Thinking of Ending Things by Iain Reid",
        description: "",
        image: "assets/reading.gif",
      },
      {
        name: "Tideland by Mitch Cullin",
        description: "",
        image: "assets/reading.gif",
      },
      { name: "Foe by Coetzee", description: "", image: "assets/reading.gif" },
    ],
    music: [
      {
        name: "Immunity by Clairo",
        description: "",
        image: "assets/music.gif",
      },
      {
        name: "I by cigarettes after sex",
        description: "",
        image: "assets/music.gif",
      },
      {
        name: "The Suburbs by Arcade Fire",
        description: "",
        image: "assets/music.gif",
      },
    ],
  },
};
