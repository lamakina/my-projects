import { emotions, activities, answers } from "./data.js";

const entry = document.getElementById("questions");

const states = {
  STATE_SELECT_EMOTION: 0,
  STATE_SELECT_ACTIVITY: 1,
  STATE_SHOW_RESULT: 2,
};

let applicationState = states.STATE_SELECT_EMOTION;
let emotion;
let activity;

const emotionSelectCallback = (value) => {
  emotion = value;
  applicationState = states.STATE_SELECT_ACTIVITY;
  updateState();
};
const activitySelectCallback = (value) => {
  activity = value;
  applicationState = states.STATE_SHOW_RESULT;
  updateState();
};

const updateState = () => {
  entry.innerHTML = "";
  if (applicationState == states.STATE_SELECT_EMOTION) {
    emotions.forEach((emotion) => {
      const button = document.createElement("button");
      button.innerText = emotion.text;
      button.onclick = () => emotionSelectCallback(emotion.value);
      entry.appendChild(button);
    });
  } else if (applicationState == states.STATE_SELECT_ACTIVITY) {
    activities.forEach((activity) => {
      const button = document.createElement("button");
      button.innerText = activity.text;
      button.onclick = () => activitySelectCallback(activity.value);
      entry.appendChild(button);
    });
  } else {
    const possibleAnswers = answers[emotion][activity];
    const random = Math.floor(Math.random() * 3);
    const answer = possibleAnswers[random];

    const title = document.createElement("h2");
    title.innerText = answer.name;
    entry.appendChild(title);

    const image = document.createElement("img");
    image.src = answer.image;
    entry.appendChild(image);

    const description = document.createElement("p");
    description.innerText = answer.description;
    entry.appendChild(description);
  }
};

updateState();
